package com.prog.alex;

import com.prog.alex.exceptions.MoneyMismatchException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class MoneyTest
{
    /**
     * Comprobar que al crear una moneda, se establece correctamente sus atributos
     */
    @Test
    void testCreateMoney() {

        Money money = new Money(125,"EUR");
        Assertions.assertEquals(money.getAmount(),125,"Amount not set on constructor");
        Assertions.assertEquals(money.getCurrencyISOCode(),"EUR","Currency ISO");

    }

    /**
     * Comprobar que podemos sumar monedas del mismo tipo
     */
    @Test
    void testAddAmountToMoneySameCurrency() {

        Money money=new Money(100,"EUR");

        Money moneyToPlus = new Money(125,"EUR");

        money.plus(moneyToPlus);

        Assertions.assertEquals(money.getAmount(),225,"Error adding money");
        Assertions.assertEquals("EUR",money.getCurrencyISOCode(),"Error different currency");



    }

    /**
     * Comprobar que si intentamos sumar monedas de distinto tipo se lanza una excepción
     */
    @Test
    void testAddAmountToMoneyMismatchCurrency() {

        final Money money=new Money(100,"EUR");

        final Money moneyToPlus = new Money(125,"USD");

        Assertions.assertThrows(MoneyMismatchException.class,() -> {money.plus(moneyToPlus);},"Exception not throw");


    }

    /**
     * Comprobar que podemos restar monedas del mismo tipo
     */
    @Test
    void testSubtractAmountToMoneySameCurrency() {

        Money money=new Money(125,"EUR");

        Money moneyToMinus = new Money(100,"EUR");

        money.minus(moneyToMinus);

        Assertions.assertEquals(25,money.getAmount(),"Error money different ammount");
        Assertions.assertEquals("EUR",money.getCurrencyISOCode(),"Error different currency");


    }

    /**
     * Comprobar que si intentamos restar monedas de distinto tipo se lanza una excepción
     */
    @Test
    void testSubtractAmountToMoneyMismatchCurrency() {

        Money money=new Money(125,"EUR");

        final Money moneyToMinus = new Money(100,"USD");

        money.minus(moneyToMinus);

        Assertions.assertThrows(MoneyMismatchException.class,() -> {money.plus(moneyToMinus);},"Exception not throw");

    }

    /**
     * Comprobar comparación de monedas iguales
     */
    @Test
    void testCompareMoneysSameCurrency() {


        Money money=new Money(100,"EUR");

        Money money1 = new Money(100,"EUR");

        Assertions.assertTrue(money.equals(money1));

    }

    /**
     * Comprobar comparación de monedas diferentes
     */
    @Test
    void testCompareMoneysMismatchCurrency() {

        Money money=new Money(100,"EUR");

        Money moneyToCompare = new Money(125,"USD");

        Assertions.assertNotEquals(money,moneyToCompare,"Comparison distinct currency does not work");

    }
}
