package com.prog.alex;

import com.prog.alex.exceptions.CurrencyNotSupportedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;



public class BankTest {



    public Bank bank;
    public Money moneyEUR=new Money(10,"EUR");
    public Money moneyGBP=new Money(10,"GBP");
    public Money moneyUSD=new Money(10,"USD");




    /**
     * Testear que todas las combinaciones posibles funcionan
     * EUR-USD, EUR-GBP...
     */
    @ParameterizedTest
    @CsvSource({ "EUR,100,USD,135"})
    void testConvertAmount(String fromCurrency,double fromAmount,String toCurrency,double toAmount) {

        Bank bank=new Bank();

        Money money=new Money(fromAmount,fromCurrency);

        Money result=bank.convert(money,toCurrency);

        Assertions.assertEquals(toCurrency,result.getCurrencyISOCode());
        Assertions.assertEquals(toAmount,result.getAmount());

    }

    /**
     * Testear que cuando intentamos convertir una moneda a otra no soportada se lanza
     * la excepción correspondiente
     */
    @Test
    void testConvertNotSupportedCurrency() {

        Bank bank=new Bank();

        final Money money=new Money(100,"EUR");

        Assertions.assertThrows(CurrencyNotSupportedException.class,() -> {bank.convert(money,"CYD");});

    }

}
