package com.prog.alex;

import com.prog.alex.exceptions.MoneyMismatchException;

public class Money
{
    private double amount;

    private String currencyISOCode;

    public Money (double amount, String currency)
    {
        this.amount = amount;
        this.currencyISOCode = currency;
    }

    public void plus (Money moneyToAdd) {
        this.checkCurrencies(moneyToAdd);
        this.amount += moneyToAdd.getAmount();
    }

    public void minus (Money moneyToSubtract) {
        this.checkCurrencies(moneyToSubtract);
        this.amount -= moneyToSubtract.getAmount();
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrencyISOCode() {
        return currencyISOCode;
    }

    private void checkCurrencies(Money money) {
        if(money.getCurrencyISOCode() != this.getCurrencyISOCode()){
            throw new MoneyMismatchException();
        }
    }

    /**
     * this method will be called when we have to compare dates
     */
    public boolean equals (Object obj)
    {
        if (obj instanceof Money) {
            if (this.getCurrencyISOCode().equals(((Money) obj).getCurrencyISOCode())) {
                return this.getAmount() == (((Money) obj).getAmount());
            }
        }
        return false;
    }
}
