package com.prog.alex;

import com.prog.alex.exceptions.CurrencyNotSupportedException;

public class Bank {

    public Money convert(Money money, String currencyIsoCode){

         double exchangeRate = this.getExchangeRate(money.getCurrencyISOCode()+"-"+currencyIsoCode);

         double newAmount =  money.getAmount()*exchangeRate;

         return new Money(newAmount, currencyIsoCode);

    }

    private double getExchangeRate(String fromToCode) {

        switch (fromToCode) {
            case "EUR-USD":
                return 1.35;
            case "EUR-GBP":
                return 0.9;
            case "USD-EUR":
                return 0.83;
            case "USD-GBP":
                return 0.74;
            case "GBP-EUR":
                return 1.13;
            case "GBP-USD":
                return 1.35;
        }

        throw new CurrencyNotSupportedException();

    }
}
