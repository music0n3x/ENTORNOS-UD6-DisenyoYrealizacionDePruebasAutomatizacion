package com.edd.test;

public class SensorStub {

    public static final double OFFSET = 16;

    public double popNextPressurePsiValueHigh()
    {
        double pressureTelemetryValue = samplePressureHigh();
        return OFFSET + pressureTelemetryValue;
    }

    private static double samplePressureHigh()
    {
        // placeholder implementation that simulate a real sensor in a real tire
        return 50;
    }

    public double popNextPressurePsiValueLow()
    {
        double pressureTelemetryValue = samplePressureLow();
        return OFFSET + pressureTelemetryValue;
    }

    private static double samplePressureLow()
    {
        // placeholder implementation that simulate a real sensor in a real tire
        return -1;
    }
    public double popNextPressurePsiValueNormal()
    {
        double pressureTelemetryValue = samplePressureNormal();
        return OFFSET + pressureTelemetryValue;
    }

    private static double samplePressureNormal()
    {
        // placeholder implementation that simulate a real sensor in a real tire
        return 3;
    }

    public double popNextPressurePsiValueHighLimit()
    {
        double pressureTelemetryValue = samplePressureHighLimit();
        return OFFSET + pressureTelemetryValue;
    }

    private static double samplePressureHighLimit()
    {
        // placeholder implementation that simulate a real sensor in a real tire
        return 5;
    }

    public double popNextPressurePsiValueLowLimit()
    {
        double pressureTelemetryValue = samplePressureLowLimit();
        return OFFSET + pressureTelemetryValue;
    }

    private static double samplePressureLowLimit()
    {
        // placeholder implementation that simulate a real sensor in a real tire
        return 1;
    }

    public double popNextPressurePsiValueUnderLowLimit()
    {
        double pressureTelemetryValue = samplePressureUnderLowLimit();
        return OFFSET + pressureTelemetryValue;
    }

    private static double samplePressureUnderLowLimit()
    {
        // placeholder implementation that simulate a real sensor in a real tire
        return 0;
    }

    public double popNextPressurePsiValueOverHighLimit()
    {
        double pressureTelemetryValue = samplePressureOverHighLimit();
        return OFFSET + pressureTelemetryValue;
    }

    private static double samplePressureOverHighLimit()
    {
        // placeholder implementation that simulate a real sensor in a real tire
        return 6;
    }




}
