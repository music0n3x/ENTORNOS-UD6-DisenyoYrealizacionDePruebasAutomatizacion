package com.edd.test;

import org.junit.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestAlarmMockito {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {

        Alarm alarm = new Alarm();
        assertFalse("Error alarm is set on by default",alarm.isAlarmOn());

    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    public void testAlarmOnWithLowPressure() {

        MockitoAlarm mockitoAlarm=new MockitoAlarm();
        mockitoAlarm.checkLow();
        assertTrue("Error alarm not activated when pressure is under stablished limit",mockitoAlarm.isAlarmOn());
    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {

        MockitoAlarm mockitoAlarm=new MockitoAlarm();
        mockitoAlarm.checkHigh();
        assertTrue("Error alarm not activated when pressure is over stablished limit",mockitoAlarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */
    @Test
    public void testAlarmOffWithNormalPressure() {

        MockitoAlarm mockitoAlarm = new MockitoAlarm();
        mockitoAlarm.checkNormal();
        assertFalse("Error alarm is activated when pressure is on stablished limits",mockitoAlarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @Test
    public void testAlarmOffWithLimitsPressure() {

        MockitoAlarm mockitoAlarm = new MockitoAlarm();
        mockitoAlarm.checkHighLimit();
        assertFalse("Error alarm is activated when pressure is on high limit",mockitoAlarm.isAlarmOn());
        mockitoAlarm.checkLowLimit();
        assertFalse("Error alarm is activated when pressure is on low limit",mockitoAlarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @Test
    public void testAlarmOnWithLimitsPressure() {

        MockitoAlarm mockitoAlarm = new MockitoAlarm();
        mockitoAlarm.checkOverHighLimit();
        assertTrue("Error alarm is not activated when pressure is over high limit",mockitoAlarm.isAlarmOn());
        mockitoAlarm.checkUnderLowLimit();
        assertTrue("Error alarm is not activated when pressure is under low limit",mockitoAlarm.isAlarmOn());

    }
    
}
