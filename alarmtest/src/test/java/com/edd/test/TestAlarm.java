package com.edd.test;

import org.junit.*;

import static org.junit.Assert.*;


public class TestAlarm {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {

        Alarm alarm = new Alarm();
        assertFalse("Error alarm is set on by default",alarm.isAlarmOn());

    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    public void testAlarmOnWithLowPressure() {

        FakeAlarm fakeAlarm = new FakeAlarm();
        fakeAlarm.checkLow();
        assertTrue("Error alarm not activated when pressure is under stablished limit",fakeAlarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {

        FakeAlarm fakeAlarm = new FakeAlarm();
        fakeAlarm.checkHigh();
        assertTrue("Error alarm not activated when pressure is over stablished limit",fakeAlarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */
    @Test
    public void testAlarmOffWithNormalPressure() {

        FakeAlarm fakeAlarm = new FakeAlarm();
        fakeAlarm.checkNormal();
        assertFalse("Error alarm is activated when pressure is on stablished limits",fakeAlarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @Test
    public void testAlarmOffWithLimitsPressure() {

        FakeAlarm fakeAlarm = new FakeAlarm();
        fakeAlarm.checkHighLimit();
        assertFalse("Error alarm is activated when pressure is on high limit",fakeAlarm.isAlarmOn());
        fakeAlarm.checkLowLimit();
        assertFalse("Error alarm is activated when pressure is on low limit",fakeAlarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @Test
    public void testAlarmOnWithLimitsPressure() {

        FakeAlarm fakeAlarm = new FakeAlarm();
        fakeAlarm.checkOverHighLimit();
        assertTrue("Error alarm is not activated when pressure is over high limit",fakeAlarm.isAlarmOn());
        fakeAlarm.checkUnderLowLimit();
        assertTrue("Error alarm is not activated when pressure is under low limit",fakeAlarm.isAlarmOn());

    }
    
}
