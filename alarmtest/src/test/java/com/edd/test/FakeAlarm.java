package com.edd.test;

public class FakeAlarm {

    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    private SensorStub sensorStub;

    private boolean alarmOn;

    public FakeAlarm(){
        this.sensorStub = new SensorStub();
        this.alarmOn = false;
    }

    public void checkHigh(){
        double psiPressureValue = sensorStub.popNextPressurePsiValueHigh();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkLow(){
        double psiPressureValue = sensorStub.popNextPressurePsiValueLow();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkNormal(){
        double psiPressureValue = sensorStub.popNextPressurePsiValueNormal();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkLowLimit(){
        double psiPressureValue = sensorStub.popNextPressurePsiValueLowLimit();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkHighLimit(){
        double psiPressureValue = sensorStub.popNextPressurePsiValueHighLimit();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkOverHighLimit(){
        double psiPressureValue = sensorStub.popNextPressurePsiValueOverHighLimit();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkUnderLowLimit(){
        double psiPressureValue = sensorStub.popNextPressurePsiValueUnderLowLimit();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }




    public boolean isAlarmOn()
    {
        return alarmOn;
    }

}
