package com.edd.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockitoAlarm {

    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    private Sensor sensor;

    private boolean alarmOn;

    public MockitoAlarm(){
        this.sensor = mock(Sensor.class);
        this.alarmOn = false;
    }

    public void checkHigh(){
        when(sensor.popNextPressurePsiValue()).thenReturn(50.0);
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkLow(){
        when(sensor.popNextPressurePsiValue()).thenReturn(-1.0);
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkNormal(){
        when(sensor.popNextPressurePsiValue()).thenReturn(19.0);
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkLowLimit(){
        when(sensor.popNextPressurePsiValue()).thenReturn(17.0);
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkHighLimit(){
        when(sensor.popNextPressurePsiValue()).thenReturn(21.0);
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkOverHighLimit(){
        when(sensor.popNextPressurePsiValue()).thenReturn(22.0);
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }
    public void checkUnderLowLimit(){
        when(sensor.popNextPressurePsiValue()).thenReturn(16.0);
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }

    public boolean isAlarmOn()
    {
        return alarmOn;
    }

}
